const { When, Then } = require("@wdio/cucumber-framework");
const { expect, $ } = require("@wdio/globals");
import InventoryPage from "../pageobjects/inventorypage";

When(/^User is on Saucedemo inventory page$/, async () => {
  await InventoryPage.open();
  browser.pause(3000);
  await expect(browser).toHaveUrl("https://www.saucedemo.com/inventory.html");
});

When(/^User click on menu button$/, async () => {
  await InventoryPage.clickMenuButton();
});

Then(/^Menu links are visible$/, async () => {
  await InventoryPage.verifyMenuLinkAreVisible();
});

Then(/^User click shopping cart link$/, async () => {
  await InventoryPage.clickShoppingCartLink();
});

When(/^User clicks an item's add to cart button$/, async () => {
  await InventoryPage.clickAddItemButton();
});

Then(/^Button text is: Remove$/, async () => {
  await InventoryPage.verifyRemoveTextInButton();
});

Then(/^Shopping cart badge is visible$/, async () => {
  await InventoryPage.verifyShoppingCartBadgeIsDisplayed();
});

Then(/^User clicks on item title$/, async () => {
  await InventoryPage.clickItemTitle();
});

Then(/^User scrolls the page$/, async () => {
  const last_item = await InventoryPage.redShirtItem();
  await InventoryPage.ScrollToLastItem();

  await expect(last_item).toBeDisplayed();
});

Then(/^All products in inventory are displayed$/, async () => {
  await InventoryPage.geItems();
});
