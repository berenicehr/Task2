const { Then } = require("@wdio/cucumber-framework");
const { expect, $ } = require("@wdio/globals");
import ItemPage from "../pageobjects/itempage";

Then(/^User is redirected to item info$/, async () => {
  await ItemPage.openTestItemPage();
  browser.pause(3000);
  await expect(browser).toHaveUrl(
    "https://www.saucedemo.com/inventory-item.html?id=4"
  );
});
