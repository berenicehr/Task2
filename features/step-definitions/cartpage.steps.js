const { Given, When, Then } = require("@wdio/cucumber-framework");
const { expect, $ } = require("@wdio/globals");
import CartPage from "../pageobjects/cartpage";

Then(/^User is on cart page$/, async () => {
  await CartPage.open();
  browser.pause(3000);
  await expect(browser).toHaveUrl("https://www.saucedemo.com/cart.html");
});

Then(/^Continue shopping and Checkout buttons are visible$/, async () => {
  await CartPage.getCartButtons();
});
