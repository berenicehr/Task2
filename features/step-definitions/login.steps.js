const { Given, When, Then } = require("@wdio/cucumber-framework");
const { expect, $ } = require("@wdio/globals");
import LoginPage from "../pageobjects/loginpage";

Given(/^User is on login page$/, async () => {
  await LoginPage.openLoginpage();
  await expect(browser).toHaveUrl("https://www.saucedemo.com/");
});

When(/^User logs valid username and password$/, async () => {
  const user = "standard_user";
  const password = "secret_sauce";
  await LoginPage.login(user, password);
});

When(/^User logs invalid username and password$/, async () => {
  const user = "invalid_user";
  const password = "secret_sauce";
  await LoginPage.login(user, password);
});

When(/^User logs with locked out credentials$/, async () => {
  const user = "locked_out_user";
  const password = "secret_sauce";
  await LoginPage.login(user, password);
});

Then(/^Invalid credentials message is visible$/, async () => {
  await LoginPage.openInvalidMessage();
});

Then(/^Locked out message is visible$/, async () => {
  await LoginPage.openInvalidMessage();
  const locked_message = await LoginPage.invalidCredentialMessage();
  await expect(locked_message).toHaveText(
    "Epic sadface: Sorry, this user has been locked out."
  );
});
