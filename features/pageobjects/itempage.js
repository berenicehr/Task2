const { $ } = require("@wdio/globals");
const Page = require("./page");

class ItemPage extends Page {
  openTestItemPage() {
    return super.open("inventory-item.html?id=4");
  }
}

module.exports = new ItemPage();
