const { browser } = require("@wdio/globals");

module.exports = class Page {
  /**
   * @param path path of the sub page (e.g. /path/to/page.html)
   */
  open(path) {
    return browser.url(`https://www.saucedemo.com/${path}`);
  }
};
