const { $ } = require("@wdio/globals");
const Page = require("./page");

class LoginPage extends Page {
  inputUsername() {
    return $('//input[@data-test="username"]');
  }

  inputPassword() {
    return $('//input[@data-test="password"]');
  }

  btnSubmit() {
    return $('//input[@id="login-button"]');
  }

  invalidCredentialMessage() {
    return $('//h3[@data-test="error"]');
  }

  login = async (username, password) => {
    const user_input = await this.inputUsername();
    const password_input = await this.inputPassword();
    const submit_button = await this.btnSubmit();
    await user_input.setValue(username);
    await password_input.setValue(password);
    await submit_button.click();
  };

  openLoginpage() {
    return browser.url(`https://www.saucedemo.com/`);
  }

  openInvalidMessage = async () => {
    const invalid_message = await this.invalidCredentialMessage();
    await browser.pause(500);

    await expect(invalid_message).toBeDisplayed();
  };
}

module.exports = new LoginPage();
