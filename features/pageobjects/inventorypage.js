const { $ } = require("@wdio/globals");
const Page = require("./page");
const assert = require("chai").assert;

class InventoryPage extends Page {
  menuButton() {
    return $("#react-burger-menu-btn");
  }

  allItemsLink() {
    return $("#inventory_sidebar_link");
  }

  aboutLink() {
    return $("#about_sidebar_link");
  }

  logoutLink() {
    return $("#logout_sidebar_link");
  }

  resetAppLink() {
    return $("#reset_sidebar_link");
  }

  shoppingCartLink() {
    return $(".shopping_cart_link");
  }

  AddItemButton() {
    return $("#add-to-cart-sauce-labs-backpack");
  }

  removeItemButton() {
    return $("#remove-sauce-labs-backpack");
  }

  shoppingCartBadge() {
    return $(".shopping_cart_badge");
  }

  itemTitleLink() {
    return $("#item_4_title_link");
  }

  redShirtItem() {
    return $('//a[@id="item_3_title_link"]');
  }

  Items() {
    return $$("//div[@class ='inventory_item']");
  }

  open() {
    return super.open("inventory.html");
  }

  clickMenuButton = async () => {
    const menu_button = await this.menuButton();
    await menu_button.click();
    await expect(menu_button).toBeClickable();
  };

  clickShoppingCartLink = async () => {
    const shopping_cart_link = await this.shoppingCartLink();
    await shopping_cart_link.click();
    await expect(shopping_cart_link).toBeClickable();
  };

  clickAddItemButton = async () => {
    const addItemButtonForTest = await this.AddItemButton();
    await addItemButtonForTest.isClickable();
    addItemButtonForTest.click();
  };

  verifyRemoveTextInButton = async () => {
    const removeButton = await this.removeItemButton();
    const removeButtonText = await removeButton.getText();
    expect(removeButtonText).toHaveText("Remove");
  };

  verifyMenuLinkAreVisible = async () => {
    const all_items_link = await this.allItemsLink();
    const about_link = await this.aboutLink();
    const log_out_link = await this.logoutLink();
    const reset_app_link = await this.resetAppLink();

    await expect(all_items_link).toBeDisplayed();
    await expect(about_link).toBeDisplayed();
    await expect(log_out_link).toBeDisplayed();
    await expect(reset_app_link).toBeDisplayed();
  };

  verifyShoppingCartBadgeIsDisplayed = async () => {
    const shoppingBadge = await this.shoppingCartBadge();
    await expect(shoppingBadge).toBeDisplayed();
  };

  clickItemTitle = async () => {
    const itemTitle = await this.itemTitleLink();
    await itemTitle.isClickable();
    itemTitle.click();
    browser.pause(3000);
  };

  ScrollToLastItem = async () => {
    const lastItem = await this.redShirtItem();
    await lastItem.scrollIntoView();
    browser.pause(2000);
  };

  geItems = async () => {
    const items = await this.Items();
    const countItems = items.length;
    assert.equal(countItems, 6, "Expected 6 items in inventory");
  };
}
module.exports = new InventoryPage();
