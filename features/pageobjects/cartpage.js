const { $ } = require("@wdio/globals");
const Page = require("./page");
const expectChai = require("chai").expect;

class CartPage extends Page {
  continueShoppingButton() {
    return $("//button[@data-test='continue-shopping']");
  }

  checkoutButton() {
    return $("//button[@data-test='checkout']");
  }

  open() {
    return super.open("cart.html");
  }

  getCartButtons = async () => {
    const continue_button = await this.continueShoppingButton();
    const checkout_button = await this.checkoutButton();

    await expect(continue_button).toBeDisplayed();
    await expect(checkout_button).toBeDisplayed();
    expectChai(continue_button).to.exist;
    expectChai(checkout_button).to.exist;
  };
}

module.exports = new CartPage();
