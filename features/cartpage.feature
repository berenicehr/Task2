Feature: Saucedemo cart page

Scenario: TC-010 User can see cartpage content 

     Given User is on login page
     And User logs valid username and password
     When User click shopping cart link
     Then Continue shopping and Checkout buttons are visible
