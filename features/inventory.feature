Feature: Saucedemo inventory page

Scenario: TC-004 User navigates swag labs menu.

     Given User is on login page
     And User logs valid username and password
     When User is on Saucedemo inventory page 
     And User click on menu button
     Then Menu links are visible

Scenario: TC-005 User clicks shopping cart link.

     Given User is on Saucedemo inventory page 
     When User click shopping cart link
     Then User is on cart page

Scenario: TC-006 User is on inventory page and clicks on Feature Item.

      Given User is on Saucedemo inventory page 
      When User clicks on item title
      Then User is redirected to item info

 
Scenario: TC-007 User press add item to cart button.

    Given User is on Saucedemo inventory page 
    When User clicks an item's add to cart button
    Then Button text is: Remove

Scenario: TC-008 User can see shopping cart badge when item is added to cart 

     Given User is on Saucedemo inventory page 
     When User clicks an item's add to cart button
     Then Shopping cart badge is visible

Scenario: TC-009 User can see all Products in Inventory

     Given User is on Saucedemo inventory page 
     When User scrolls the page
     Then All products in inventory are displayed


     

