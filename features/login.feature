Feature: Saucedemo login page

  Scenario: TC-001 User login with valid credentials 

    Given User is on login page 
    When User logs valid username and password
    Then User is on Saucedemo inventory page
  

  Scenario: TC-002 User login with invalid credentials 

    Given User is on login page 
    When User logs invalid username and password
    Then Invalid credentials message is visible 
    

  Scenario: TC-003 User login with a locked out user credentials

    Given User is on login page 
    When User logs with locked out credentials
    Then Locked out message is visible 




